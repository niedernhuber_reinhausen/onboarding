# README #

This is a __sandbox__ type repo, specifically for on-boarding our new student interns in development roles. 

## Chapter 1: Git ##

### Task 1: Clone this git repository to your desktop  ###
Use the <code>git clone https://YOUR-BITBUCKET-USERNAME@bitbucket.org/niedernhuber_reinhausen/onboarding.git</code> command 
or your preferred graphical git tool. You can get the link and clone command by pressing the Clone button in Bitbucket.
Depending on how you set up Docker, it might be easier for you, if clone to a location inside WSL. 

### Task 2: Checkout the playground branch 
Use the <code>git checkout playground</code> command. You should now see a zoo folder in your local repository.

### Task 3: Create your own feature branch from the playground branch ###
Pick animal you like and create a branch, in order to add it to the zoo.  
Use the <code>git checkout -b feature/YOUR_ANIMALS_NAME_GOES_HERE playground</code> command to branch off from the
playground branch.   The *feature/* part of the branch is the convention we use at MR for naming branches, that 
are intended to add new functionality. We also use *bugfix/* for bugfixes and *release/* for releases. 

### Task 4: Add a .txt file with the name of your animal in the zoo folder ###
Create the file using the editor of your choice, then <code>git add</code> the file to your local staging environment.
Now <code>git commit</code> your changes. Remember to choose a meaningful commit message.
Finally, <code>git push</code> your commit to remote. You might have to use <code> --set-upstream origin </code> flag.

### Task 5: Create a pull request in Bitbucket to merge your feature branch into playground ###
Use the *Create pull request* button in Bitbucket to open a new pull request. Make your animal's branch is set 
as the source branch and the playground branch is set as the destination branch.  
Give your pull request a meaningful title and description.  
Set Ben, Helena and a fellow new intern as the reviewers for your PR. Make sure everyone is someone else's reviewer.  
Check the *Delete branch after pull request is merged* checkbox and then press the create pull request button. 

### Task 6: Review the pull request, that you've been assigned to as Code Reviewer. ###
You can find all the pull requests, that you've been assigned to review in Bitbucket under the pull requests tab,
when filtering for *Reviewing*. Normally you would thoroughly check the code for mistakes, look 
for coding guideline violations or other mishaps.  
But since this is not real code, you can just add a friendly comment
about the animal and click the *Approve* button. 

### Task 7: Merge your pull request ###
After you have the necessary approval, you can merge your pull request by pressing the merge button. 
In practice, you might not have the necessary right perform a merge, so someone from your team will merge
your pull requests after the necessary checks have all passed.

### Task 8: Add a description to your animal's txt file. ###
Create a new branch for the description, add a description to the .txt you previously created, then commit and push 
it. You can just copy & paste the wikipedia description or literally write anything. Don't start with Task 9 until everyone is finished with Task 8.

### Task 9 Create and try to merge a PR for the animal description you wrote. ###
Commit and push the description you wrote. Then open a pull request.
You should notice that the pull request can't be merged, because there are conflicts. This is because we conflicted your pull request, by making changes to the same file. 

### Task 10 Fix the merge conflict ###
Make sure the branch, with the description you wrote is checked out, using the <code> git checkout </code>
command. Use the <code> git merge origin/playground </code> command to start merging. Git will tell you that
there are conflicts you need to fix. You can use your favorite editor and open the conflicting file. Then carefully
compare both of the conflicting versions and either use your version, the other version or a combined version
of the description.   
In the context of this exercise it doesn't really matter which option you choose. In practice,
you should pay close attention when merging and make sure the resulting code works and contains everything necessary
from both versions. The resulting file should not contain any of lines inserted by git like *<<<<<<< HEAD*, *=======* or
*>>>>>>> branch_name*.  
When you're finished commit and push your changes.

### Task 11 Do something stupid ###
On any branch of your choosing, write something horribly stupid in any file. Then commit your changes, but don't push them. 

### Task 12 Undo something stupid ###
Thankfully you noticed your commit is absolutely terrible, before you pushed it.
Use the <code> git reset --hard HEAD~1</code> command to undo your last horrendous commit.
This command will delete the commit as well as your local changes.  
If you wanted to undo multiple commits here, you
could also do that by changing the number after *HEAD~*, for example <code>git reset --hard HEAD~5</code> 
will undo the last five commits. Should you want to undo your commit, but keep your local changes for further editing,
you can use <code> git reset HEAD~1</code> without the *--hard* flag.

## Chapter 2: Docker ##

### Task 13 Switch to the *docker-land* branch and docker-compose up the docker-compose file there ###
Use <code> git checkout docker-land</code> to switch to the *docker-land* branch.
There is a docker-compose file called *docker-compose-sandbox.yml* in there. 
Use the <code> docker-compose -f "docker-compose-sandbox.yml" up </code> to spin up the containers specified
in the compose file automatically.  
If you only had a single docker-compose file called "docker-compose.yml", you could just call <code> docker compose up </code>
without specifying the file name. 

### Task 14 Stop and start the docker-compose instance ###
Use the <code> docker-compose -f "docker-compose-sandbox.yml" stop </code> to halt the containers without
removing them.
Now use the <code> docker-compose -f "docker-compose-sandbox.yml" start </code> to start your containers again. 
You can also pause and unpause the containers, if you want them to temporarily not do anything but stay in their
current state using <code> docker-compose -f "docker-compose-sandbox.yml" pause </code> and
<code> docker-compose -f "docker-compose-sandbox.yml" unpause </code>.
Perform all of these commands once. 

### Task 15 Get a Shell into a running container ###
Use the <code> docker ps </code> command to get a list of running containers. Pick a container and remember its container id
or the first couple of characters of the id. 
Then use the <code> docker exec --it YOUR_CONTAINER_ID /bin/bash </code> command to open a bash shell into the container.
Then use the <code> ls </code> command to get an overview of the containers file system. Use <code> exit</code> or
crtl+c to get out of the shell. 

### Task 16 Remove all the containers
Use the <code> docker-compose -f "docker-compose-sandbox.yml" down </code> command to remove all the running
container from the docker-compose instance. Then use <code> docker volume prune</code> and <code> docker image prune</code> to remove all the remaining now unused volumes and images from your system. 
